FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive
ENV HOME /root
ENV TZ=Australia/Brisbane
ENV LANG=C.UTF-8

RUN echo "export TZ=$TZ" >> $HOME/.bashrc
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN echo "export LANG=$LANG" >> $HOME/.bashrc
RUN mkdir -p $HOME/bin
RUN echo "export PATH=$HOME/bin:\$PATH" >> $HOME/.bashrc

COPY etc $HOME

# GHC/ghcup 9.0.2
RUN apt-get update \
    && \
    apt-get install -y --no-install-recommends \
    build-essential \
    ca-certificates \
    curl \
    libffi-dev \
    libgmp-dev \
    libgmp10 \
    libncurses-dev \
    libncurses5\
    libtinfo5 \
    pkg-config \
    wget \
    zlib1g-dev
RUN curl --proto '=https' --tlsv1.2 -sSf https://get-ghcup.haskell.org | env BOOTSTRAP_HASKELL_NONINTERACTIVE=1 sh
RUN echo "export PATH=\$HOME/.cabal/bin:\$HOME/.ghcup/bin:\$PATH" >> $HOME/.bashrc
ENV PATH "${HOME}/.cabal/bin:${HOME}/.ghcup/bin:${PATH}"
RUN ghcup install ghc 8.6
RUN ghcup install ghc 8.10
RUN ghcup install ghc 9.4
RUN ghcup install ghc 9.10
RUN ghcup set ghc 9.4
RUN ghcup install cabal 3.0
RUN ghcup install cabal 3.8
RUN ghcup install cabal 3.10
RUN ghcup set cabal 3.8
RUN cabal update
RUN cabal install pandoc-cli
RUN cabal install --lib lens

RUN apt-get update \
    && \
    apt-get install -y --no-install-recommends \
    apt-utils \
    bash \
    context \
    csstidy \
    default-jdk \
    imagemagick \
    ghostscript \
    git \
    libreoffice \
    libreoffice-java-common \
    libwebp-dev\
    lmodern \
    openssh-server \
    pdftk \
    poppler-utils \
    qpdf \
    rsync \
    telnet \
    texlive-extra-utils \
    texlive-luatex \
    texlive-science \
    texlive-xetex \
    ttf-dejavu\
    snap \
    sudo \
    tidy \
    w3c-linkchecker \
    vim \
    webp \
    wget
RUN sed -i 's/\(<policy domain="coder" rights=\)"none" \(pattern="PDF" \/>\)/\1"read|write"\2/g' /etc/ImageMagick-6/policy.xml

# msfonts
RUN apt-get update \
    && \
    apt-get install -y --no-install-recommends \
    apt-utils \
    rsync
ENV FONT_DIR "/usr/share/fonts/truetype"
RUN mkdir -p ${FONT_DIR}
RUN find $HOME/msfonts -name "*.[Tt][Tt][Ff]" -print0 | rsync -aH --files-from=- --from0 ./ ${FONT_DIR}/

# draw.io
ENV DRAWIO_VERSION 24.7.17
RUN apt-get update \
    && \
    apt-get install -y --no-install-recommends \
    apt-utils \
    curl \
    dbus-x11 \
    desktop-file-utils \
    libasound-dev \
    libatspi2.0-0 \
    libgbm-dev \
    libgtk-3.0 \
    libnotify4 \
    libsecret-1.0 \
    libxss1 \
    rsync \
    ssh \
    wget \
    xvfb
RUN wget --no-check-certificate https://github.com/jgraph/drawio-desktop/releases/download/v${DRAWIO_VERSION}/drawio-amd64-${DRAWIO_VERSION}.deb
RUN apt-get install -y --no-install-recommends ./drawio-amd64-${DRAWIO_VERSION}.deb

# test commands
RUN echo $PATH
RUN cabal --version
# w3c-linkchecker
RUN checklink --version
# ImageMagick
RUN convert --version
RUN curl --version
RUN display --version
RUN ghc --version
RUN ghcup --version
RUN git --version
# ghostscript
RUN gs --version
RUN java --version
RUN javac --version
RUN libreoffice --version
# ImageMagick
RUN mogrify --version
RUN pandoc --version
RUN pdftk --version
RUN qpdf --version
RUN rsync --version
RUN wget --version
RUN pdfjam --version
RUN pdfinfo -v
# dbus for draw.io
# RUN xvfb-run -a drawio --disable-gpu --headless --no-sandbox --version
COPY scripts/start-dbus.sh /usr/local/bin
RUN mkdir -p /var/run/dbus
ENTRYPOINT ["/bin/bash", "/usr/local/bin/start-dbus.sh"]
# draw.io
# Example: xvfb-run -a drawio --export --format pdf --output ${OUTPUT}.pdf example.drawio --disable-gpu --headless --no-sandbox
